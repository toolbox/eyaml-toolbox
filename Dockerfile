FROM containers.ligo.org/toolbox/igwn-toolbox:el9

RUN dnf -y groupinstall "Development Tools" && \
    dnf -y install ruby ruby-devel python3-devel  && \
    dnf clean all

RUN gem install hiera && \
    gem install hiera-eyaml && \
    gem install hiera-eyaml-gpg && \
    gem install gpgme && \
    gem install yamllint
